dev:
	python3 ./server/manage.py runserver

debug:
	python3 ./server/manage.py shell

db:
	python3 ./server/manage.py makemigrations
	python3 ./server/manage.py migrate

dump:
	python3 ./server/manage.py dumpdata studyarchive --output=./data.json

load:
	python3 ./server/manage.py loaddata ./data.json

locale:
	django-admin makemessages -l en_Us
	django-admin makemessages -l zh_Hans

translate:
	rm -rf ./server/locale/*/LC_MESSAGES/django.mo
	django-admin compilemessages

admin:
	python3 ./server/manage.py createsuperuser