# StudyArchive

## 简介

用于管理学习资料和学习进度的简易WEB应用

## 技术架构

1. Django
2. Django REST Framework
3. SQLite
4. GraphQL
5. Protobuf
6. Gitee
7. taro

## 发布渠道

1. PyPI
2. Gitee
3. DockerHub

## 功能设计

1. 直接使用Django的admin后台管理系统做数据管理。
2. 本地使用SQLite数据库存储数据。
3. 将数据编码成Protobuf格式，通过Gitee开放API上传到Gitee仓库保存。
4. 基于Django REST Framework构建GraphQL接口，提供数据上传/下载/同步、导入导出功能。
5. 采用taro构建调用GraphQL接口的用户界面，以便于后续迁移至移动端。

## 后续规划

1. 进行交互式数据可视化。
2. 引入AI丰富功能，提升用户体验。
