from django.utils.translation import gettext_lazy as _
from rest_framework.routers import DefaultRouter
from .views import TaskViewSet, SourceViewSet, TagViewSet, ProjectViewSet

router = DefaultRouter()
router.register(r"task", TaskViewSet, basename=_("task"))
router.register(r"source", SourceViewSet, basename=_("source"))
router.register(r"tag", TagViewSet, basename=_("tag"))
router.register(r"project", ProjectViewSet, basename=_("project"))
