from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class StudyarchiveConfig(AppConfig):
    default_auto_field: str = "django.db.models.BigAutoField"
    name: str = "studyarchive"
    verbose_name: str = _("studyarchive")
