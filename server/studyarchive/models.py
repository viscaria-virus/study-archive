from django.db import models
from django.db.models import UniqueConstraint, BaseConstraint
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from enum import Enum, auto


class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("name"), unique=True)
    reference = models.URLField(verbose_name=_("reference"))
    introduction = models.TextField(blank=True, verbose_name=_("introduction"))
    category = models.CharField(max_length=50, verbose_name=_("category"), blank=True)

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self) -> str:
        return reverse("admin:studyarchive_tag_change", args=[self.id])

    class Meta:
        verbose_name: str = _("tag")
        verbose_name_plural: str = _("tag")


class Source(models.Model):
    class Category(models.IntegerChoices):
        PLATFORM = 0, _("platform")
        OFFICIAL_WEBSITE = 1, _("official_website")
        OTHER = 2, _("other")

    name = models.CharField(max_length=200, verbose_name=_("name"), unique=True)
    url = models.URLField(verbose_name=_("url"), unique=True)
    category = models.SmallIntegerField(
        choices=Category, verbose_name=_("category"), default=Category.OTHER
    )

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self) -> str:
        return reverse("admin:studyarchive_source_change", args=[self.id])

    class Meta:
        verbose_name: str = _("source")
        verbose_name_plural: str = _("source")


class Task(models.Model):
    class Status(models.IntegerChoices):
        READY: tuple[int, str] = 0, "🕒 " + _("ready")
        IN_PROGRESS: tuple[int, str] = 1, "🔄 " + _("in_progress")
        FINISHED: tuple[int, str] = 2, "✅ " + _("finished")
        UNFINISHED: tuple[int, str] = 3, "⚠️ " + _("unfinished")
        PLANNED: tuple[int, str] = 4, "📅 " + _("planned")

    class Category(models.IntegerChoices):
        VIDEO: tuple[int, str] = 0, "▶️ " + _("video")
        DOCUMENT: tuple[int, str] = 1, "📑 " + _("document")
        PLAYGROUND: tuple[int, str] = 2, "🎡 " + _("playground")
        BOOK: tuple[int, str] = 3, "📕 " + _("book")
        EXERCISE: tuple[int, str] = 4, "📋 " + _("exercise")
        ARTICLE: tuple[int, str] = 5, "📝 " + _("article")

    class Review(models.IntegerChoices):
        NORMAL: tuple[int, str] = 0, "🤷 " + _("normal")
        GOOD: tuple[int, str] = 1, "👍 " + _("good")
        BAD: tuple[int, str] = 2, "👎 " + _("bad")

    class Action(Enum):
        FORWARD = auto()
        BACKWARD = auto()
        START = auto()
        FINISH = auto()
        PAUSE = auto()
        RESET = auto()

    name = models.CharField(max_length=100, verbose_name=_("name"))
    url = models.URLField(verbose_name=_("url"))
    source = models.ForeignKey(
        Source, on_delete=models.SET_NULL, null=True, verbose_name=_("source")
    )
    tags = models.ManyToManyField(
        Tag, through="TaskTag", related_name="tags", verbose_name=_("tag"), blank=True
    )
    status = models.SmallIntegerField(
        choices=Status, verbose_name=_("status"), default=Status.PLANNED
    )
    category = models.SmallIntegerField(choices=Category, verbose_name=_("category"))
    course = models.BooleanField(verbose_name=_("course"), default=True)
    review = models.SmallIntegerField(
        choices=Review, null=True, blank=True, verbose_name=_("review")
    )

    def __str__(self) -> str:
        return f"{self.name}-{self.source}"

    def get_absolute_url(self) -> str:
        return reverse("admin:studyarchive_task_change", args=[self.id])

    def change_status(self, action: int):
        def transfer_state(state: int, reverse: bool) -> int:
            if state == Task.Status.UNFINISHED:
                return Task.Status.IN_PROGRESS
            if reverse and state == Task.Status.PLANNED:
                return state
            if state == Task.Status.FINISHED:
                return state
            step: int = -1 if reverse else 1
            return (state + step) % 5

        match action:
            case Task.Action.FORWARD:
                self.status = transfer_state(self.status, False)
            case Task.Action.BACKWARD:
                self.status = transfer_state(self.status, True)
            case Task.Action.START:
                self.status = Task.Status.IN_PROGRESS
            case Task.Action.FINISH:
                self.status = Task.Status.FINISHED
            case Task.Action.PAUSE:
                self.status = Task.Status.UNFINISHED
            case Task.Action.RESET:
                self.status = Task.Status.PLANNED
        self.save()

    class Meta:
        verbose_name: str = _("task")
        verbose_name_plural: str = _("task")
        constraints: tuple[BaseConstraint, ...] = (
            UniqueConstraint(fields=["name", "url"], name="unique_task"),
        )


class TaskTag(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)


class Project(models.Model):
    class Priority(models.IntegerChoices):
        URGENT: tuple[int, str] = 0, "🟥 " + _("urgent")
        HIGH: tuple[int, str] = 1, "🟨 " + _("high")
        NORMAL: tuple[int, str] = 2, "🟦 " + _("normal")
        LOW: tuple[int, str] = 3, "🟩 " + _("low")

    class Target(models.IntegerChoices):
        SKILL: tuple[int, str] = 0, "🛠️ " + _("skill")
        KNOWLEDGE: tuple[int, str] = 1, "📚 " + _("knowledge")
        INSPIRATION: tuple[int, str] = 2, "💡 " + _("inspiration")

    name = models.CharField(max_length=100, verbose_name=_("name"), unique=True)
    tasks = models.ManyToManyField(
        Task, through="ProjectTask", related_name="tasks", verbose_name=_("task")
    )
    priority = models.SmallIntegerField(
        choices=Priority, verbose_name=_("priority"), default=Priority.NORMAL
    )
    target = models.SmallIntegerField(
        choices=Target, verbose_name=_("target"), default=Target.SKILL
    )
    introduction = models.TextField(blank=True, verbose_name=_("introduction"))

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name: str = _("project")
        verbose_name_plural: str = _("project")


class ProjectTask(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
