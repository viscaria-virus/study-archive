from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.admin.options import InlineModelAdmin
from django.db.models import Q
from django.db.models.query import QuerySet
from django.http import HttpRequest
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from .models import Tag, Source, Task, Project

LINK_TEMPLATE = """<!--html-->
<a href="{}" target="_blank" style="display: inline-block; max-width: 200px; overflow: hidden; text-overflow: ellipsis;">{}</a>
"""


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display: tuple[str, ...] = ("name", "_reference", "category")
    list_display_links: tuple[str, ...] = ("name",)
    list_editable: tuple[str, ...] = ("category",)
    list_filter: tuple[str | SimpleListFilter, ...] = ("category",)
    search_fields: tuple[str, ...] = ("name",)

    @admin.display(description=_("reference"))
    def _reference(self, obj: Tag) -> str:
        return format_html(LINK_TEMPLATE, obj.reference, obj.reference)


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    list_display: tuple[str, ...] = ("name", "_url", "category")
    list_display_links: tuple[str, ...] = ("name",)
    list_filter: tuple[str | SimpleListFilter, ...] = ("category",)
    search_fields: tuple[str, ...] = ("name",)
    list_per_page: int = 20

    @admin.display(description=_("url"))
    def _url(self, obj: Source) -> str:
        return format_html(LINK_TEMPLATE, obj.url, obj.url)


class TagInline(admin.TabularInline):
    model = Task.tags.through
    extra: int = 0
    verbose_name: str = _("tag")
    verbose_name_plural: str = _("tag")
    classes: tuple[str, ...] = ("collapse",)
    raw_id_fields: tuple[str, ...] = ("tag",)


class SourceFilter(admin.SimpleListFilter):
    title: str = _("source")
    parameter_name: str = "source"

    def lookups(
        self, request: HttpRequest, model_admin: admin.ModelAdmin
    ) -> list[tuple[int, str]]:
        options = Source.objects.exclude(
            Q(category=Source.Category.OFFICIAL_WEBSITE)
            | Q(category=Source.Category.OTHER)
        )
        others = Source.objects.filter(category=Source.Category.OTHER)
        return (
            [(option.id, option.name) for option in options]
            + [(-1, _("official_website"))]
            + [(option.id, option.name) for option in others]
        )

    def queryset(
        self, request: HttpRequest, queryset: QuerySet[Source]
    ) -> QuerySet[Source] | None:
        result: QuerySet[Source] = None
        match self.value():
            case "-1":
                result = queryset.filter(
                    source__category=Source.Category.OFFICIAL_WEBSITE
                )
            case None:
                result = queryset.all()
            case _:
                result = queryset.filter(source__id=self.value())
        return result


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display: tuple[str, ...] = (
        "name",
        "category",
        "_url",
        "status",
        "review",
        "_source",
    )
    list_display_links: tuple[str, ...] = ("name",)
    list_editable: tuple[str, ...] = ("status", "review")
    inlines: tuple[InlineModelAdmin, ...] = (TagInline,)
    exclude: tuple[str, ...] = ("tags",)
    list_filter: tuple[str | SimpleListFilter, ...] = (
        "category",
        "status",
        "review",
        SourceFilter,
        "course",
    )
    search_fields: tuple[str, ...] = ("name",)
    list_per_page: int = 20
    actions: tuple[str, ...] = (
        "forward",
        "backward",
        "start",
        "finish",
        "pause",
        "reset",
    )

    @admin.display(description=_("url"))
    def _url(self, obj: Task) -> str:
        return format_html(LINK_TEMPLATE, obj.url, obj.url)

    @admin.display(description=_("source"))
    def _source(self, obj: Task) -> str:
        return format_html(LINK_TEMPLATE, obj.source.get_absolute_url(), obj.source)

    @admin.action(description=_("go_to_the_next_step"))
    def forward(self, request: HttpRequest, queryset: QuerySet[Task]):
        for task in queryset:
            task.change_status(Task.Action.FORWARD)

    @admin.action(description=_("go_back_to_the_previous_step"))
    def backward(self, request: HttpRequest, queryset: QuerySet[Task]):
        for task in queryset:
            task.change_status(Task.Action.BACKWARD)

    @admin.action(description=_("start_the_tasks"))
    def start(self, request: HttpRequest, queryset: QuerySet[Task]):
        for task in queryset:
            task.change_status(Task.Action.START)

    @admin.action(description=_("finish_the_tasks"))
    def finish(self, request: HttpRequest, queryset: QuerySet[Task]):
        for task in queryset:
            task.change_status(Task.Action.FINISH)

    @admin.action(description=_("pause_the_tasks"))
    def pause(self, request: HttpRequest, queryset: QuerySet[Task]):
        for task in queryset:
            task.change_status(Task.Action.PAUSE)

    @admin.action(description=_("reset_task_status"))
    def reset(self, request: HttpRequest, queryset: QuerySet[Task]):
        for task in queryset:
            task.change_status(Task.Action.RESET)


class TaskInline(admin.TabularInline):
    model = Project.tasks.through
    extra: int = 0
    verbose_name: str = _("task")
    verbose_name_plural: str = _("task")
    classes: tuple[str, ...] = ("collapse",)
    raw_id_fields: tuple[str, ...] = ("task",)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display: tuple[str, ...] = ("name", "progress", "target", "priority")
    list_editable: tuple[str, ...] = ("target", "priority")
    inlines: tuple[InlineModelAdmin, ...] = (TaskInline,)
    exclude: tuple[str, ...] = ("tasks",)
    list_filter: tuple[str | SimpleListFilter, ...] = ("priority", "target")
    search_fields: tuple[str, ...] = ("name",)
    ordering: tuple[str, ...] = ("priority",)
    list_per_page: int = 10
    actions: tuple[str, ...] = ("clear",)

    @admin.display(description=_("progress"))
    def progress(self, obj: Project) -> str:
        total = len(obj.tasks.all())
        finished = len(obj.tasks.filter(status=Task.Status.FINISHED))
        return f"{finished}/{total}" if finished < total else "✅"

    def get_queryset(self, request: HttpRequest) -> QuerySet[Project]:
        return super().get_queryset(request).prefetch_related("tasks")

    @admin.action(description=_("clear_finished_tasks"))
    def clear(self, request: HttpRequest, queryset: QuerySet[Project]):
        for project in queryset:
            project.tasks.set(project.tasks.exclude(status=Task.Status.FINISHED))
            if project.tasks.count() == 0:
                project.delete()
