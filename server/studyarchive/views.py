from rest_framework import viewsets
from rest_framework.pagination import LimitOffsetPagination, BasePagination
from rest_framework.serializers import Serializer
from .models import Task, Source, Tag, Project
from .serializers import (
    TaskSerializer,
    SourceSerializer,
    TagSerializer,
    ProjectSerializer,
)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class: Serializer = TaskSerializer
    pagination_class: BasePagination = LimitOffsetPagination


class SourceViewSet(viewsets.ModelViewSet):
    queryset = Source.objects.all()
    serializer_class: Serializer = SourceSerializer
    pagination_class: BasePagination = LimitOffsetPagination


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class: Serializer = TagSerializer
    pagination_class: BasePagination = LimitOffsetPagination


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class: Serializer = ProjectSerializer
    pagination_class: BasePagination = LimitOffsetPagination
