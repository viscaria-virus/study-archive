from django.db import models
from rest_framework import serializers
from .models import Task, Source, Tag, Project


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model: models.Model = Task
        fields: tuple[str, ...] = (
            "id",
            "name",
            "url",
            "source",
            "tags",
            "status",
            "category",
            "course",
            "review",
        )


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model: models.Model = Source
        fields: tuple[str, ...] = ("id", "name", "url", "category")


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model: models.Model = Tag
        fields: tuple[str, ...] = (
            "id",
            "name",
            "reference",
            "introduction",
            "category",
        )


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model: models.Model = Project
        fields: tuple[str, ...] = ("id", "name", "tasks", "priority", "introduction")
